FROM node:23.4.0-slim


RUN apt update && \
    apt install -y git gpg jq curl && \
    npm install -g \
        semantic-release@24.2.0 \
        @semantic-release/gitlab@13.2.3 \
        @semantic-release/git@10.0.1 \
        @semantic-release/changelog@6.0.3 \
        @semantic-release/exec@6.0.3

COPY scripts/* /usr/local/bin/